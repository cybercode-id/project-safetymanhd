using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine;

public class objectTrigger : MonoBehaviour
{

    public bool onTrigger = true;
    /// <summary>
    /// Unity Events invoked when colliding.
    /// <summary>

    public UnityEvent enterEvent;
    public UnityEvent leaveEvent;


    void OnTriggerEnter(Collider other)
    {
        if (onTrigger)
        {
            if (other.tag == "Player")
            {
                enterEvent.Invoke();
            }
        }
        else
        {
            return;
        }
        

        //do something here directly,
        //or assign event methods in the inspector

        
    }

    void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            leaveEvent.Invoke();
        }
    }




    /// <summary>
    ///  Applies an explosion force to the colliding object.
    /// </summary>
}
